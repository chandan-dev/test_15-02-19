#include<stdio.h>
#include<conio.h>
int print_reverse(int size, int array[size])
{
	int i;
	int j = 0;
	int result[size];
	for(i = size-1 ; i >= 0; i--)
	{
		result[j] = array[i];
		j++;
	}
	printf("Array in reverse order :");
	for(i = 0; i <size; i++)
	{
		printf("%d  ", result[i]);
	}
	return 0;
}
int main(int arg1, char *arg2[])
{
	int i, size, test_case;
	printf("Enter number of test caes :\n");
	scanf("%d", &test_case);
	printf("Size of array:\n");
	scanf("%d",&size);
	int array[size];
	printf("Enter %d input tothe array: \n");
	for(i = 0; i < size; i++)
	{
		scanf("%d", &array[i]);
	}
	 print_reverse(size, array);	
}

